# README #

# Prerequisites #

Before using the Vagrant provisioning included in this repository, you will need to have Git, VirtualBox, and Vagrant installed.

### Download and Install Git ###
* [git link](http://git-scm.com/)

### Download and Install VirtualBox ###
* [virtualbox link](https://www.virtualbox.org/wiki/Downloads)

### Download and Install Vagrant ###
* [Vagrant link](http://downloads.vagrantup.com/)

### Clone this repo ###
* [repo link](git clone https://ruelsison@bitbucket.org/ruelsison/simplewave_gcc.git)

### Starting vagrant ###
* run console application in windows (cmd.exe / powershell.exe)
* Change to the repository directory with file containing Vagrant.
* run vagrant up
* run vagrant ssh

### Compiling the source code using ARM GCC ###
* Change directory to cd /vagrant/simpleband
* run make