#ifndef _H_ALGORITHM_H_
#define _H_ALGORITHM_H_

#include "main_includes.h"

#define ST_REST_VERTICAL 0xA0
#define ST_REST_HORIZONTAL 0xA1
#define ST_REST_UNKNOWN 0xA2
#define ST_MOTION_UNKNOWN 0xA3
#define ST_MOTION_WALKING 0xA4

typedef uint8_t stateType_t;

typedef struct _stStateParams
{
	float fAcceX;
	float fAcceY;
	float fAcceZ;
	float fAcceYZ;
	double dAccelR;
	double dGyroR;
	uint8_t ucMaxSpike;
	uint8_t ucSpikeCount;
	uint8_t ucMotionTimeout;
	stateType_t prevState;
} stStateParams_t;

uint8_t getCurrentState(stStateParams_t *stStateParams);
int checkChangedState(stateType_t iCurrState);

#endif
