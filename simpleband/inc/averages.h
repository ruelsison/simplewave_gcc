//averages.h

void updateAverages(void);
float latestAccelResultant(void);
float latestGyroResultant(void);
void initAverages(float val);

float getActivityLevel(void);
void updatePeakAverages(void);
