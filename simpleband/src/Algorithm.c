
#include "main_includes.h"
#include "Algorithm.h"
#include "LEDS.h"
#include "ActivityLog.h"

#define TH_SAMPLES_TILL_STABLE 8

#define TH_ACCE_UPPER_ONE		1.2
#define TH_ACCE_LOWER_ONE		0.7
#define TH_ACCE_UPPER_ZERO		0.7
#define TH_ACCE_LOWER_ZERO		-0.2
#define TH_GYRO_UPPER			10
#define TH_GYRO_LOWER			-10
#define TH_GYRO_UNKNOWN_STATE	50
#define TH_SPIKE_DETECT			1.4
#define TH_WALK_SPIKE_COUNT		5

#define SET_MOTION_TIMEOUT		150

#define IS_WITHIN_BOUNDS(x,upper,lower) (((x) < (upper)) && ((x) > (lower)))


/*****************************************
 * 		      STATIC FUNCTIONS
 *****************************************/


static void setUprightStateSignal(void)
{
	setOrangeLED(SWITCH_ON);
	setRedLED(SWITCH_OFF);
	setYellowLED(SWITCH_OFF);

}

static void setMotionStateSignal(void)
{
	setOrangeLED(SWITCH_ON);
	setRedLED(SWITCH_ON);
	setYellowLED(SWITCH_ON);
}


static void setUnknownStateSignal(void)
{
	setOrangeLED(SWITCH_OFF);
	setRedLED(SWITCH_OFF);
	setYellowLED(SWITCH_ON);
}

static void setRestUnknownStateSignal(void)
{
	setOrangeLED(SWITCH_ON);
	setRedLED(SWITCH_ON);
	setYellowLED(SWITCH_OFF);
}

static void setRestHorizontalStateSignal(void)
{
	setOrangeLED(SWITCH_OFF);
	setRedLED(SWITCH_ON);
	setYellowLED(SWITCH_OFF);
}


/*****************************************
 *           API FUNCTIONS
 *****************************************/

uint8_t getCurrentState(stStateParams_t *stStateParams)
{
	stateType_t currState = ST_MOTION_UNKNOWN;

	if ( IS_WITHIN_BOUNDS(stStateParams->dAccelR, TH_ACCE_UPPER_ONE, TH_ACCE_LOWER_ONE)
	  && IS_WITHIN_BOUNDS(stStateParams->dGyroR, TH_GYRO_UPPER, TH_GYRO_LOWER) )
	{
		if((stStateParams->prevState == ST_MOTION_UNKNOWN) || (stStateParams->prevState == ST_MOTION_WALKING))
		{
			if(stStateParams->ucMaxSpike > TH_SPIKE_DETECT)
			{
				if (stStateParams->ucSpikeCount < TH_WALK_SPIKE_COUNT)
				{
					stStateParams->ucSpikeCount++;
				}
				else
				{
					currState = ST_MOTION_WALKING;
					setMotionStateSignal();
					stStateParams->ucMotionTimeout = SET_MOTION_TIMEOUT;
				}
			}

			stStateParams->ucMaxSpike = 0;
			stStateParams->ucMotionTimeout--;

			if(stStateParams->ucMotionTimeout == 0)
			{
				/* reset to default */
				currState = ST_REST_VERTICAL;
				stStateParams->ucMotionTimeout = SET_MOTION_TIMEOUT;
				stStateParams->ucSpikeCount = 0;
			}

		}
		else
		{
			if ( IS_WITHIN_BOUNDS(stStateParams->fAcceX, TH_ACCE_UPPER_ONE, TH_ACCE_LOWER_ONE) )
			{
				currState = ST_REST_VERTICAL;
				setUprightStateSignal();
			}
			else if ( IS_WITHIN_BOUNDS(stStateParams->fAcceX, TH_ACCE_UPPER_ZERO, TH_ACCE_LOWER_ZERO))
			{
				currState = ST_REST_HORIZONTAL;
				setRestHorizontalStateSignal();
			}
			else
			{
				currState = ST_REST_UNKNOWN;
				setRestUnknownStateSignal();

			}
		}
	}

	if (currState != ST_MOTION_WALKING)
	{
		if (stStateParams->dGyroR > TH_GYRO_UNKNOWN_STATE)
		{
			currState = ST_MOTION_UNKNOWN;
			setUnknownStateSignal();
			stStateParams->ucMotionTimeout = SET_MOTION_TIMEOUT;
		}

		if (stStateParams->dAccelR >= TH_ACCE_UPPER_ONE)
		{
			currState = ST_MOTION_UNKNOWN;
			setUnknownStateSignal();
			stStateParams->ucMotionTimeout = SET_MOTION_TIMEOUT;
			//flagSpike = 1;
			//countSpike++;
		}

		if (stStateParams->dAccelR <= TH_ACCE_LOWER_ONE)
		{
			currState = ST_MOTION_UNKNOWN;
			setUnknownStateSignal();
			stStateParams->ucMotionTimeout = SET_MOTION_TIMEOUT;
		}
	}

	if(stStateParams->dAccelR > stStateParams->ucMaxSpike)
	{
		stStateParams->ucMaxSpike = stStateParams->dAccelR;
	}

	return currState;
}


int checkChangedState(stateType_t iCurrState)
{
	static int iCnt = 0;
	static stateType_t iPrevState = ST_REST_UNKNOWN;
	static stateType_t iTransState = ST_REST_UNKNOWN;
	int iStateIsChanged = 0;

	if (iTransState != iCurrState)
	{
		iTransState = iCurrState;
		iCnt = 0;
	}
	else
	{
		if (iPrevState != iTransState)
		{
			if (TH_SAMPLES_TILL_STABLE <= iCnt)
			{
				iPrevState = iTransState;
				iStateIsChanged = 1;
				iCnt = 0;
			}
			else
			{
				iCnt++;
			}
		}
	}

	return iStateIsChanged;
}



