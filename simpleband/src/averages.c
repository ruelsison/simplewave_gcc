//averages.c
#include "main_includes.h"
#include "averages.h"
#include "accelerometer.h"
#include "data_storage.h"
#include "coefficients.h"
#include "math.h"

float accelLatestR;
float gyroLatestR;
float sv_currAve; //2second average
float sv_2minAve; //2minute average
float sv_hourAve; 
float sv_8hrAve; 
float sv_dayAve; 

float sv_2minPeak; //2minute peak

extern float xCurr;
extern float yCurr;
extern float zCurr;
extern float yzResultant;

//20Hz Processing
#define FACTOR_CUR_AVE  	(1.0/NUMBER_OF_ENTRIES) //SMOOTHING_FACTOR_SV_DEFAULT
#define FACTOR_MIN_AVE  	(FACTOR_CUR_AVE/60)

// every 2 minutes
#define FACTOR_HOUR_AVE 	(1.0/30)
#define FACTOR_8HOUR_AVE 	(FACTOR_HOUR_AVE/8)
#define FACTOR_DAY_AVE 		(FACTOR_HOUR_AVE/24)

void updateAverages(void)
{
	Sensor_Reading *rdg = rawDataPeek();
	
	accelLatestR = getR(rdg->val.x_ac,rdg->val.y_ac,rdg->val.z_ac);
	gyroLatestR = getR(rdg->val.x_gy,rdg->val.y_gy,rdg->val.z_gy);
	
	xCurr = CONVERT_TO_G(rdg->val.x_ac);
	yCurr = CONVERT_TO_G(rdg->val.y_ac);
	zCurr = CONVERT_TO_G(rdg->val.z_ac);
	
	yzResultant = sqrt(yCurr*yCurr+zCurr*zCurr);
	
	sv_currAve = FACTOR_CUR_AVE*accelLatestR + (1.0-FACTOR_CUR_AVE)*sv_currAve;
	sv_2minAve = FACTOR_MIN_AVE*accelLatestR + (1.0-FACTOR_MIN_AVE)*sv_2minAve;
	
	if ( (accelLatestR > sv_2minPeak) ) // && (latestR < getPerso()->impact_threshold) )
	{
		sv_2minPeak = accelLatestR;
	}
	
}

void updatePeakAverages(void)
{
	
	sv_hourAve = FACTOR_HOUR_AVE*sv_2minPeak + (1.0-FACTOR_HOUR_AVE)*sv_hourAve;
	sv_8hrAve = FACTOR_8HOUR_AVE*sv_2minPeak + (1.0-FACTOR_8HOUR_AVE)*sv_8hrAve;
	sv_dayAve = FACTOR_DAY_AVE*sv_2minPeak + (1.0-FACTOR_DAY_AVE)*sv_dayAve;
	
	sv_2minPeak = 0;
	
}

void initAverages(float val)
{
	sv_currAve = val;
	sv_2minAve = val;
	sv_hourAve = 1;
	sv_8hrAve = 1;
	sv_dayAve = 1;
	
	sv_2minPeak = 0;
}

float latestAccelResultant(void)
{
	return accelLatestR;
}

float latestGyroResultant (void)
{
	return gyroLatestR;
}

float averageResultant (void)
{
    return sv_currAve;
}

float getActivityLevel(void)
{
	return sv_2minPeak;
}

